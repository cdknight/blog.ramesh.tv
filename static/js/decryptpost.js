// This file's code quality is ***HORRIBLE***.
// TODO code clean-up and actually use Promises and whatnot

async function decryptPost() {

	var pubkeyPrompt = async () => {
		await pgpKeyPrompt("Define PGP Public Key",
					`This key is used to verify that the secret post is actually from the original writer.
					<br><br>Make sure there is no trailing whitespace at the beginning
					or end of the PGP key, or decryption will fail.`,
					"Paste PGP public key here",
					"textarea",
					() => {
						// Take them back to the home page if the cancel pasting
						// in a PGP key
						window.href = "/";
					},
					async (textareaData) => {
						window.localStorage.setItem("post_verification_pubkey", textareaData);
						if (!window.sessionStorage.getItem("post_decryption_privkey_password")){
							await privkeyPasswordPrompt();
						}
						else {
							await updateDecryptPost();
						}
					});
	};

	var privkeyPasswordPrompt = async (incorrect = false) => {
		await pgpKeyPrompt("PGP Private Key Password",
					(incorrect ? `That key-pair combination and/or password didn't work.
					Try a different password and/or clear localStorage and try again.<br><br>` : "") +
					`Please input the password for this PGP private key.
					If there is no password for this PGP private key, press 'Accept.'`,
					"Input password here",
					"password",

					() => {
						window.location.href = "/";
					},
					async (passwordData) => {
						window.sessionStorage.setItem("post_decryption_privkey_password", passwordData);
						// TODO keep it DRY
						try {
							// We want to handle if the password is wrong
							await updateDecryptPost();
						}
						// TODO, y'know, actually, like check what kind of exception
						// it is and provide an appropriate error message
						catch (exception) {
							await privkeyPasswordPrompt(true);
						}
					}
		);
	}

	var updateDecryptPost = async () => {
		const verificationKey = await openpgp.readKey({ armoredKey: window.localStorage.getItem("post_verification_pubkey")});
		const decryptionKey = await openpgp.decryptKey({
			privateKey: await openpgp.readPrivateKey({ armoredKey: window.localStorage.getItem("post_decryption_privkey") }),
			passphrase: window.sessionStorage.getItem("post_decryption_privkey_password")
		});

		var title = document.querySelector("#blogpost_title");
		var description = document.querySelector("#blogpost_description");
		var content = document.querySelector(".post-content");


		[title, description, content].forEach(async (encryptedTag, index) => {
			var message = await openpgp.readMessage({ armoredMessage: encryptedTag.innerHTML });

			const { data: decrypted, signatures } = await openpgp.decrypt({
				message,
				verificationKeys: verificationKey,
				format: 'armored',
				expectSigned: true, // force signature verification
				decryptionKeys: decryptionKey,
			});

			encryptedTag.innerHTML = decrypted;

			if (encryptedTag == title) {
				// Process when title is replaced (since this is async)
				document.querySelector("title").innerHTML = title.innerHTML;
			}

		})
	};

	// Keeping things DRY since this happens both after we define
	// a new private and if a private key is defined but other things haven't been
	// defined yet.
	var promptControlFlow = async () => {
		if (!window.localStorage.getItem("post_verification_pubkey")) {
			await pubkeyPrompt();
		}
		else if (!window.sessionStorage.getItem("post_decryption_privkey_password")){
			await privkeyPasswordPrompt();
		}
		else {
			try {
				// We want to handle if the password is wrong
				await updateDecryptPost();
			}
			catch (exception) {
				await privkeyPasswordPrompt(true);
			}
		}
	};

	if (!window.localStorage.getItem("post_decryption_privkey")) {
		await pgpKeyPrompt("Define PGP Private Key",
					 `This key is used to decrypt the secret post.
					  <br><br>Make sure there is no trailing whitespace at the beginning
                      or end of the PGP key, or decryption will fail.`,
					 "Paste PGP private key here",
					 "textarea",
					 () => {
						 // Take them back to the home page if the cancel pasting
						 // in a PGP key

						 window.location.href = "/";
					 },
					 async (textareaData) => {
					 	 window.localStorage.setItem("post_decryption_privkey", textareaData);
						 promptControlFlow();
					 });
	}
	else {
		promptControlFlow();
	}



};

// areaType can either be 'password' or 'textarea'
async function pgpKeyPrompt(promptTitle, promptSubtitle, areaPlaceholder, areaType, cancelAction, acceptAction) {
	var header = document.querySelector(".blog-header");
	var content = document.querySelector(".blog-post");
	var body = document.querySelector("body");

	header.style.opacity = "0.4";
	content.style.opacity = "0.4";

	var modalElement = document.createElement("div");
	modalElement.className = "pgp-modal";

	modalElement.innerHTML = "<h1>" + promptTitle + "</h1>"
	modalElement.innerHTML += "<h4>" + promptSubtitle + "</h1>"

	var inputObject = null;
	var acceptClosure = () => {
		closeModal();
		acceptAction(inputObject.value);
	};

	if (areaType === 'textarea') {
		inputObject = document.createElement("textarea");
		inputObject.placeholder = areaPlaceholder;
		inputObject.className = "pgp-key-input";
	}
	else if (areaType === 'password') {
		inputObject = document.createElement("input");
		inputObject.placeholder = areaPlaceholder;
		inputObject.className = "pgp-key-password-input";
		inputObject.type = 'password';
		inputObject.onkeypress = (event) => {
			// Enter
			if (event.keyCode === 13) {
				acceptClosure();
			}
		}
	}


	modalElement.appendChild(inputObject);

	var buttonRowElement = document.createElement("div");
	buttonRowElement.className = 'pgp-modal-button-row';

	var closeModal = () => {
		header.style.opacity = "1.0";
		content.style.opacity = "1.0";

		modalElement.remove();
	};

	var cancelButton = document.createElement("button");
	cancelButton.innerHTML = "Cancel"
	cancelButton.classList.add("pgp-modal-button");
	cancelButton.classList.add("pgp-modal-cancel");
	cancelButton.onclick = () => {
		closeModal()
		cancelAction();
	}

	var acceptButton = document.createElement("button");
	acceptButton.innerHTML = "Accept"
	acceptButton.classList.add("pgp-modal-button");
	acceptButton.classList.add("pgp-modal-accept");
	acceptButton.onclick = acceptClosure;

	buttonRowElement.appendChild(cancelButton);
	buttonRowElement.appendChild(acceptButton);

	modalElement.append(buttonRowElement);

	body.appendChild(modalElement);
}
