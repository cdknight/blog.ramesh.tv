function toggleDarkMode() {
    var html = document.querySelector("html");
    var toggler = document.querySelector("#dark-toggler");
	console.log(toggler);

    if (html.classList.contains("darkmode")) {
        html.classList.remove("darkmode"); // This class controls whether or not we see a dark-mode page.
        replaceHljsCSS("light");
        toggler.innerHTML = "Dark Mode";
        // This saves their preference for future visits
	window.localStorage.setItem("blog_theme_dark", false);
    }
    else {
        html.classList.add("darkmode");
        replaceHljsCSS("dark");
        toggler.innerHTML = "Light Mode";
	window.localStorage.setItem("blog_theme_dark", true);
    }
}

function replaceHljsCSS(darkLight) {
    var head = document.querySelector("#hljsCSS")
    head.href = "/static/js/node_modules/@highlightjs/cdn-assets/styles/atelier-lakeside-" + darkLight + ".min.css"
}

window.onload = function() {
    // Turn on dark mode if the user pefers it automatically
    if (window.matchMedia("(prefers-color-scheme: dark)").matches || 
	    window.localStorage.getItem("blog_theme_dark") === "true") {
        toggleDarkMode();
    }
    else {
        replaceHljsCSS("light")
    }
}
