Turbolinks.setProgressBarDelay(0); // Make it show the progress bar every time.

// Thanks to https://sebastiaanluca.com/blog/4qw7ra/highlightjs-with-turbolinks!

hljs.initHighlightingOnLoad()

document.addEventListener("turbolinks:load", function() {
    document.querySelectorAll("pre code").forEach((block) => {
        console.log("highlighting blocks")
        hljs.highlightBlock(block)
    })
    document.querySelectorAll("pre.src").forEach((block) => {
        console.log("highlighting org blocks")
        hljs.configure({ languages: [ block.getAttribute("lang") ] })
        hljs.highlightBlock(block)
    })
})
