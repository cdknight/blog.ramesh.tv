require 'logging'

module BlogConfig
  BLOG_TITLE = "> Ramesh's Blog"
  BLOG_DOMAIN = 'https://ramesh.tv'
  BLOG_DESCRIPTION = 'Ramblings of https://gitlab.com/cdknight'

  BLOG_OUTPUT = './output'
  BLOG_ENCRYPTED_OUTPUT = './posts/encrypted_output'

  BLOG_GPG_DECRYPTION_EMAIL = 'blog@ramesh.tv'
  BLOG_GPG_SIGNING_EMAIL = 'me@ramesh.tv'
end

Logging.color_scheme(
  'report',
  levels: {
    info: :blue,
    debug: :green,
    warn: :yellow,
    error: :red,
    fatal: %i[white on_red]
  },
  date: :green,
  logger: :red,
  message: :blue
)

Logging.appenders.stdout(
  'stdout',
  layout: Logging.layouts.pattern(
    pattern: " (%l) %c: %m\n",
    color_scheme: 'report'
  )
)

Logging.logger.root.appenders = 'stdout'
Logging.logger.root.level = :info
