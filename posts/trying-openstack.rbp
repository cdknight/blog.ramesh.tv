(
  metadata title, "Trying OpenStack"
  metadata description, "It's a struggle \(for me\)."
  metadata tags, ["Homelab", "Computers"]
  metadata time, "2020-07-08 11:34"
  metadata language, "en"
)

OpenStack is a private-cloud platform \(I think\). Which is exactly why I have been wanting to
deploy it into my homelab. After all, why not? Sometimes I want to share my resources,
having a private cloud would be a much nicer way to do that, as opposed to something like Proxmox \(which is what I'm using at the moment\).
After all, I would imagine OpenStack has quotas, better authentication integration, et cetera, et cetera. Having something like AWS but not AWS
(italic "would") be nice.

So, off I went, trying to deploy it. I had a spare desktop lying around, so I thought I could try it to just get something up and running so I could get a feel for OpenStack, before
deploying it on something like my main server which still has Proxmox. Unfortunately, this computer has a lot of issues. In the past when I've tried to use it for testing stuff,
it basically doesn't work \(read: it crashes a lot for no reason, (italic "maybe") something to do with the airflow since it's a mini-ITX machine\).

This machine has had Proxmox installed on it for the past few months, joined as part of a cluster to the main Proxmox server, but it crashed so much I just migrated everything to the main server anyway, and I ended up
effectively keeping it on just so that Proxmox would stop complaining about the "quorum" not being met. Any OS that I put on it before that... well, there was some kind of issue. XCP-ng, which if I recall,
is based on CentOS 8? Nope, because Xen issues. Ubuntu? Well, sure, but it was prone to the random crashes like XCP-ng. So no. Effectively, anything that I threw at this machine didn't work.
The CPU (italic "is") a Ryzen 3 \(first generation, at that, a 1300X\), so maybe that also contributes to the issues I'm having.

Either way, I decided I wanted to try OpenStack again on this machine. I (italic "had") kind-of gotten it to work with DevStack on the Ubuntu install, but that didn't work too well. For example,
I couldn't get DevStack to work with NFS for whatever reason, so I was limited to the 128GB SSD in the machine, which is not much if you're testing VMs.
Furthermore, there were networking issues, effectively preventing me from having my VMs access the internet. And, as I mentioned, it crashed a lot.

Ideally, my way of doing this is to get the system up and running, and (italic "then") learn how it works, configure it, et cetera. Unfortunately, there are way too many ways
to deploy OpenStack. For example, here is a list of some that I can think of off the top of my head:

(list
    item "Kolla-ansible"
    item "Openstack-ansible"
    item "Microstack"
    item "TripleO"
    item "DevStack"
    item "Manual install"
    item "Juju charms"
    item "Packstack"
list "end")

That's quite a few, unfortunately. And being a beginner, I had absolutely no clue which one to use. I had already tried out DevStack, whose purpose I think is for OpenStack developers.
Next, I wanted to try Kolla-ansible. Of course, it requires two NICs, one for the management interface, I think, and one for the VMs you create to access the internet. I'm not exactly sure.
I still don't know much about OpenStack, since it's very hard for me to learn things without trying it myself... Needless to say, that didn't work, since I don't have two NICs in my mini-ITX
dekstop, and it only has one PCI slot for a GPU. I (italic "do") have a NIC that provides what Kolla-ansible wants, but... no room, unfortunately.

I also ruled out Openstack-ansible since it seems to not support the latest release, Ussuri, very well.
I also learned that Packstack works to make an all-in-one OpenStack deployment. I wiped my Debian 10 install,
installed CentOS 8 \(which, because there was no Xen, worked\). And... I tried Packstack, which did not work.
I think it had something to do with (code "/etc/puppet/hiera.conf") not existing, which caused the rest of the
install to fail. Of course, I know nothing about Puppet, other than what it was, so with a lot of googling I
put in my own configuration. At which point Packstack refused to honor its own answer file and I had to dump
the entire file into a the (code "common.yaml") file which is linked to (code "hiera") somehow.

However... the answer file does have some dynamically-generated answers, and, well, it basically didn't work.

At this point, I just want a functional cloud to play with. No, I'm not learning Puppet or Ansible \(yet\)
because I just want (italic "something") functional. Like... I would try to learn things if... the basic
product actually worked. I also did try the TripleO quickstart, which got as far as "deploying the overcloud"
and hung here \(like, I gave it a good 2.5 hours\). Effectively, there's nothing like the all-in-one ISO-preinstalled OpenStack as far as I can see,
and it takes a lot of effort to get where I want to be with it.

I (italic "will") continue trying, namely with Microstack, and, well, removing the GPU and trying to run the server headless with
two NICs for Kolla-ansible, for which I never actually got past the configuration file. But at this point, I don't particularly forecast much good happening. At this point, I might as well try and develop my own API
using libvirt with quotas and all. Hopefully... that works.

But it isn't looking good for OpenStack and me. Hopefully, I'll persevere one day, but it might also end up like oVirt
\(which I never got far with since it refused to work with my NFS share for little reason\). As for now, ironically,
the GPU in the mini-ITX PC is going away, and I'll be putting in the NIC for good. The little PC is going to become
a VyOS router, and maybe, if that works, I'll be writing about a success story soon.
