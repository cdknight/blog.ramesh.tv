---
title: Setting up FreeIPA for a homelab
description:  Featuring role-based access controls and self-service using Keycloak and Mokey
tags: 
time: 2021-12-31 19:48
language: en
hidden: true
...

Yes, homelabbing is cool. But when you have thirty different self-hosted web services with thirty different (or the same) username and password, it gets annoying. Really, really annoying. Because if you have one username and one password, and you use it for everything, well, what if you want to change the password? Time to do the same rote procedure of logging in, changing your password password, logging out, logging into the next service, and so on. Annoying. And if you have thirty different passwords, well, hey, that's some dandy security right there.

When you forget one of those passwords though… maybe not. Oh, and don't forget the cost of setting up thirty new user accounts for when you want to share your self-hosted services with someone. Kind-of annoying, right?

** What's the solution?
When researching solutions for this problem a long time ago, I first discovered what an LDAP server was—a centralised directory of users, which also handled password management (okay, this definition might be wrong, but that's what I've gotten out of my experiences). There were solutions that integrated these into nice packages: Active Directory, FreeIPA, and I'm sure there are others. I wanted two things, though. They were:

1. Most popular services should be able to authenticate against my centralised solution. Services usually have one of two things: OpenID Connect/OAuth2, or straight authentication against the LDAP server. Whatever I chose should be able to *support /both/*.
2. Users should be able to reset their passwords. If they forget their password, I wanted a *self-service portal* so they can just reset it, or change whatever they need from there.

FreeIPA by itself provides the LDAP authentication, obviously. Mokey is a good self-service application for FreeIPA. Fun fact: I actually wrote my own FreeIPA self-service application in Flask a few years ago, but the source code is somewhere and I'm too lazy to find it (/suuuuuuure you wrote that, Ramesh/—but I really did and I'll update this post when I feel like digging it out).

I spent a lot of time trying to find the best SSO solution for OpenID Connection/OAuth2, and even now I'm not sure if I made the best choice. Here are your choices (and I'm sure there are tons more), if you're curious. I needed two things with my SSO solution, though. One was role-based access control, where I could add someone to a group in FreeIPA and restrict access to services based on that. The second one was that it respected my password expiration settings.

1. ORY Hydra + Mokey. Hydra provides the SSO stuff but Mokey provides the frontend. To be honest, I'm still not very sure how OAuth2 works (and I should be...), but basically you log in on Mokey and Hydra does some magic and lets you into the application. Trivial to modify and add password expiration support and role-based access control
2. Dex. Dex provides OpenID Connect support and OAuth, and SAML, but their SAML is basically deprecated due to security bugs. Although… I don't think I was going to use SAML anyway.
3. Authelia has OpenID Connect support as well and I think its own authentication protocol.
4. Keycloak is a bloated Java-based application that can pull from SSSD, Kerberos, or LDAP (FreeIPA supports all of those), but a lot of people use it and it has pretty good documentation/support, over any of the other solutions. But it doesn't have RBAC without a plugin.

   I eventually settled for Keycloak since it would probably save time in the long run, and I don't have a lot of time at the moment. The idea that I could simply set up something that worked well enough was really appealing to me. Unfortunately, getting Keycloak to work perfectly was not at all easy, but I eventually prevailed. This blog post will help you set integrate Keycloak and FreeIPA and set up all the things I failed to set up (which included forking and hacking together something from someone's Keycloak plugin).
