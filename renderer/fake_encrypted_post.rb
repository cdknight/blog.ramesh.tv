require './renderer/base_post'
require './config'

require 'yaml'

# fake an encrypted post when we don't have a GPG environment
# eg. in output
class FakeEncryptedPost < BasePost
  def initialize(location)

    metadata_yaml = YAML.load(
      File.read(File.join(BlogConfig::BLOG_ENCRYPTED_OUTPUT, location, "metadata.yml")),
      permitted_classes: [DateTime, Time, Symbol]
    )

    @metadata = metadata_yaml.merge(**{
      'title' => "Secret Post",
      'description' => "Secret posts are private and not publicly viewable."
    })

    @title_url = File.basename location

    # this gets overwritten anyway
    @body = "<div class='fake-post'></div>"

    super
  end

  def title_url
    @title_url
  end
end
