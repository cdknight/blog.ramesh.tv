require 'digest'

class BasePost
  attr_accessor :data, :metadata, :body
  attr_reader :encrypted

  def initialize(pagestr)
    @logger = Logging.logger[self]
  end

  def encrypted=(encrypted)
    @encrypted = encrypted
    # set title_url since the sha256 of the encrypted title
    # is not a fixed value, which causes with SEO(?)/url sharing
    crypto = GPGME::Crypto.new
    @encrypted_title_url = Digest::SHA256.hexdigest @metadata['title'].to_url
  end

  def title_url # TODO inheritance
    if self.encrypted
      @encrypted_title_url
    else
      @metadata['title'].to_url
    end
  end

  def preview
    @body.gsub(/<.+?>/, '').split(" ")[..19].join(' ')
  end
end
