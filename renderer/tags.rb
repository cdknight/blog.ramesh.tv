class Tags
  attr_accessor :tags

  def initialize(posts)
    @posts = posts
    @tags = []

    @posts.each do |post|
      @tags += post.metadata['tags']
    end

    @tags = @tags.uniq
  end
end
