class TagsPage < Index
  def initialize(_posts, template, tags)
    @logger = Logging.logger[self] # reuse code?
    @posts = []
    tags.tags.each do |tag|
      info = { 'title' => tag }
      info['description'] = Tag.tag_description tag
      @posts << info
    end

    @template = template
    @metadata = { 'title' => 'Tags', 'decription' => '', type: ['home'] }
    @body = Nokogiri::XML::NodeSet.new Nokogiri::HTML('<div></div>')
    @type = 'tag'

    populate_body
  end
end
