require './renderer/base_post'
require 'pandoc-ruby'
require 'yaml'

class PandocPost < BasePost
  def initialize(pagestr, type)
    super(pagestr)

    unless PandocRuby::READERS.keys.include? type
      type = { # Try converting based on some popular extensions
        'md' => :markdown,
        'mw' => :mediawiki,
        'tex' => :latex
      }[type]
      puts type
    end

    metadata = YAML.load(pagestr).map do |k, v|
      [k.downcase, v] # Make sure the keys are all lowercase for future processing
    end.to_h
    pagestr = pagestr.gsub(/---$(\n|.)+\.\.\.$/, '') # Remove the YAML

    @metadata = metadata.merge(**{
                                 'type': 'post',
                                 'time' => DateTime.parse(metadata['time']),
                                 'tags' => metadata['tags']&.split(', ') || []
                               })
    # this is how you know it's time to rewrite your SSG in rust
    if @metadata['hidden']
      @metadata['hidden'] = 'true'
    end

    @body = "<div class='pandoc-post'>#{PandocRuby.new(pagestr, from: type, to: :html)}</div>"
  end
end
