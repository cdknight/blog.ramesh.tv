require './renderer/post'
require './renderer/tags'
require './renderer/tag'
require './renderer/rss'
require './renderer/index'
require './renderer/base'
require './renderer/tags_page'
require './renderer/adoc_post'
require './renderer/pandoc_post'
require './renderer/fake_encrypted_post'
require './config'

require 'fileutils'
require 'colorize'
require 'find'
require 'sassc'
require 'logging'
require 'gpgme'

class Render
  def initialize(only_encrypted_output: false, output: false)
    # Multilang regex /^([^.]+)\.\w+\.rbp/
    @only_encrypted_output = only_encrypted_output
    @output = output

    @logger = Logging.logger[self]
    @posts = Dir.new('./posts').children
                .select { |f| File.file? File.join('./posts', f) }
                .map do |post_path|
                  case post_path.split('.')[-1]
                  when 'rbp'
                    next if @only_encrypted_output
                    Post.new File.read(File.join('./posts', post_path))
                  when 'adoc'
                    next if @only_encrypted_output
                    AsciiDocPost.new File.read(File.join('./posts', post_path))
                  when 'gpg'
                    next if @output
                    render_encrypted_post File.join('./posts', post_path)
                  else
                    next if @only_encrypted_output
                    PandocPost.new File.read(File.join('./posts', post_path)), post_path.split('.')[-1]
                  end
                end
                .filter { |post| post != nil } # both render:output and render:encrypted will generate a nil post
                .filter { |post| post.metadata['hidden'] != 'true'}

    if @output
      @encrypted_posts = Dir.new(BlogConfig::BLOG_ENCRYPTED_OUTPUT).children
                          .map do |encrypted_post_path|
        FakeEncryptedPost.new encrypted_post_path
      end

      @posts += @encrypted_posts
    end

    @tags = Tags.new @posts
    @posts.each do |post|
      @logger.debug "Post metadata is #{post.metadata}"
    end
  end

  def render
    clean
    npm_install

    render_posts
    copy_encrypted_posts
    render_scss
    render_index
    render_tags
    render_tags_page
    render_rss


    @logger.info 'all done.'
  end

  def copy_encrypted_posts
    if @output
      # copy the partially-encrypted HTML files that were created with
      # render:encrypted
      @logger.info "copying encrypted HTML files to posts"
      Dir.new(BlogConfig::BLOG_ENCRYPTED_OUTPUT).children.each do |encrypted_post|
        @logger.info "   copying #{encrypted_post} to ./output/post"
        FileUtils.cp_r File.join(BlogConfig::BLOG_ENCRYPTED_OUTPUT, encrypted_post),
                       File.join(BlogConfig::BLOG_OUTPUT, 'post')
      end
    end
  end

  # nothing is actually using full_clean at the moment
  def clean(full_clean: false)
    if @only_encrypted_output || full_clean
      @logger.info 'cleaning the encrypted_output directory'
      FileUtils.rm_rf BlogConfig::BLOG_ENCRYPTED_OUTPUT
    else
      @logger.info 'cleaning the output directory'
      FileUtils.rm_rf BlogConfig::BLOG_OUTPUT
    end
  end

  def npm_install
    @logger.info 'installing js modules'
    `npm i --prefix static/js`
  end

  def render_encrypted_post(post_path)

    # the key that is used to sign is also the same key used to encrypt the original file
    # we will use the signing key to decrypt the original file,
    # re-encrypt it with the decryption key, and then sign it with the signing key
    gpg_executor = GPGME::Crypto.new armor: true
    gpg_encryptor = GPGME::Crypto.new recipients: BlogConfig::BLOG_GPG_DECRYPTION_EMAIL, sign: true, armor: true

    # test the blog PGP key, for re-encryption
  

    post = nil

    begin
      gpg_encryptor.encrypt "Test"
      begin 
        GPGME::Ctx.new do |ctx|
          decrypted_post = (gpg_executor.decrypt File.open(post_path)).read

          post = case post_path.split(".")[-2]
          when "rbp"
              Post.new decrypted_post
          when "adoc"
              AsciiDocPost.new decrypted_post
          else
              PandocPost.new decrypted_post, post_path.split('.')[-2]
          end
          post.encrypted = true

        end
      rescue GPGME::Error::NoSecretKey
        @logger.warn "Could not find PGP keys to decrypt secret post"
      end
    rescue
      @logger.warn "GPG key for encryption doesn't work, skipping encrypted post"
    end


    post


  end

  def render_posts
    output = @only_encrypted_output ? BlogConfig::BLOG_ENCRYPTED_OUTPUT : File.join(BlogConfig::BLOG_OUTPUT, 'post')

    @logger.info 'exporting posts'
    @posts.each do |post|
      @logger.info "  exporting post #{post.title_url}.html"
      begin 
        base = Base.new(File.read('./templates/base.rbt'), post, @tags)
      rescue GPGME::Error::InvalidValue
        @logger.warn "Cannot encrypt post, skipping"
        next
      end

      filename = File.join(output, post.title_url, 'index.html')

      FileUtils.mkdir_p File.dirname filename unless File.directory? File.dirname filename

      File.open(filename, 'w') do |file|
        file.write base.html
      end

      if @only_encrypted_output
        # copy metadata to yaml file
        metadata_filename = File.join(output, post.title_url, 'metadata.yml')
        File.open(metadata_filename, 'w') do |file|
          metadata_copy = post.metadata
          metadata_copy.delete "description"
          metadata_copy.delete "title"

          file.write YAML.dump metadata_copy
        end
      end
    end
  end

  def render_scss
    @logger.info 'exporting static & scss/sass'
    FileUtils.cp_r './static', BlogConfig::BLOG_OUTPUT

    Find.find(File.join(BlogConfig::BLOG_OUTPUT, 'static')).each do |file|
      next unless File.file? file

      next unless file.match?(/.*\.(scss|sass)/) && !file.match?(/node_modules/)

      @logger.info "  compiling scss/sass file #{file}"
      File.write file.gsub(/(scss|sass)/, 'css'), SassC::Engine.new(File.read(file), style: :compressed).render
      FileUtils.rm file
    end
  end

  def render_index
    @logger.info 'exporting index.html'
    index_filename = File.join(BlogConfig::BLOG_OUTPUT, 'index.html')
    File.open(index_filename, 'w') do |file|
      index = Index.new @posts.reject { |post|
        post.metadata['language'].strip != 'en'
      }, File.read('./templates/postlisting.rbt')
      base = Base.new(File.read('./templates/base.rbt'), index, @tags)
      file.write base.html
    end
  end

  def render_tags
    @logger.info 'exporting tags'
    @tags.tags.each do |tag_name|
      tag = Tag.new @posts, File.read('./templates/postlisting.rbt'), tag_name
      base = Base.new(File.read('./templates/base.rbt'), tag, @tags)

      tag_filename = File.join(BlogConfig::BLOG_OUTPUT, 'tag', tag_name.downcase, 'index.html')

      FileUtils.mkdir_p File.dirname tag_filename unless File.directory? File.dirname tag_filename

      File.open(tag_filename, 'w') do |file|
        file.write base.html
      end
    end
  end

  def render_tags_page
    @logger.info 'exporting tags page'
    tags_page_filename = File.join(BlogConfig::BLOG_OUTPUT, 'tags', 'index.html')

    FileUtils.mkdir_p File.dirname tags_page_filename unless File.directory? File.dirname tags_page_filename

    File.open(tags_page_filename, 'w') do |file|
      index = TagsPage.new @posts, File.read('./templates/tagslisting.rbt'), @tags
      base = Base.new(File.read('./templates/base.rbt'), index, @tags)
      file.write base.html
    end
  end

  def render_rss
    @logger.info 'generating rss'
    posts_rss = PostsRSS.new @posts
    File.open('./output/rss.xml', 'w') do |file|
      file.write posts_rss.body
    end
  end
end
