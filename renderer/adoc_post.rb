require 'asciidoctor'
require 'stringex'
require './renderer/base_post'

class AsciiDocPost < BasePost
  def initialize(pagestr)
    loaded = Asciidoctor.load pagestr, safe: :safe

    @metadata = {
      'type': 'post',
      'title' => loaded.title,
      'description' => loaded.attr('description'),
      'time' => DateTime.parse(loaded.attr('time')),
      'language' => loaded.attr('language'),
      'tags' => loaded.attr('tags').split(', ')
    }
    @body = "<div class='asciidoc-post'>#{loaded.convert}</div>"

    super
  end
end
