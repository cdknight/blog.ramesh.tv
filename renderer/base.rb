require './config'
require 'nokogiri'
require 'gpgme'

class Base
  def initialize(basestring, post, tags)
    @logger = Logging.logger[self]
    @base = Nokogiri::HTML(basestring)
    @post = post
    @tags = tags

    @logger.debug "post is #{post.metadata}"

    if post.encrypted
      @crypto = GPGME::Crypto.new recipients: BlogConfig::BLOG_GPG_DECRYPTION_EMAIL,
                                  sign: true,
                                  armor: true
    end

    update_base
  end

  def html
    @base.to_html
  end

  def update_base
    if @post.encrypted
      # encrypt (some of) the post metadata and body
      @post.metadata['title'] = (@crypto.encrypt @post.metadata['title']).read
      @post.metadata['description'] = (@crypto.encrypt @post.metadata['description']).read
      @post.body = (@crypto.encrypt @post.body).read
    end

    title = @base.at_css 'blogtitle'
    title.name = 'title'
    title.content = @post.metadata['title']
    @logger.debug "title is now #{@post.metadata['title']}"

    headertags = @base.at_css 'headertags'
    headertags_parent = headertags.parent
    headertags.remove

    @tags.tags.each do |tag|
      tag_node = Nokogiri::XML::Node.new headertags['tag'], @base
      tag_node['class'] = headertags['class']
      tag_node << "<a href='/tag/#{tag.downcase}'>#{tag}</a>"

      headertags_parent << tag_node
    end

    rsstags = @base.at_css 'rsstags'
    rsstags_parent = rsstags.parent
    rss_node = Nokogiri::XML::Node.new headertags['tag'], @base
    rss_node['class'] = headertags['class']
    rss_node << "<a href='/rss.xml'>RSS</a>"
    rsstags_parent << rss_node
    rsstags.remove

    blogheadertitle = @base.at_css 'blogname'
    blogheadertitle.name = 'h1'
    blogheadertitle.content = BlogConfig::BLOG_TITLE

    blogposttitle = @base.at_css 'blogposttitle'
    @logger.debug @post.metadata
    if @post.metadata[:type] != 'home'
      blogposttitle.name = 'div'
      blogposttitle << "<h1 id='blogpost_title'>#{@post.metadata['title']}</h1>"
      blogposttitle << "<p id='blogpost_description'>#{@post.metadata['description']}</p>"
      @logger.debug 'blogposttitle' + blogposttitle.to_html
    end

    if @post.metadata['time']
      time_formatted = @post.metadata['time'].strftime '%B %d, %Y at %H:%M'
      blogposttitle << "<p id='blogpost_time'>#{time_formatted}</p>"
    end

    pagecontent = @base.at_css 'pagecontent'
    pagecontent.name = 'div'

    if @post.metadata[:type].instance_of?(Array)
      classnames = @post.metadata[:type].map { |v| v + '-content' }.join(' ')
    elsif @post.metadata[:type].instance_of?(String)
      classnames = @post.metadata[:type] + '-content'
    end
    pagecontent << "<div class='#{classnames}'>#{@post.body}</div>#{"<script>decryptPost()</script>" if @post.encrypted}"
  end
end
