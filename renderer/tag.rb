require './renderer/index'
require 'logging'

class Tag < Index
  attr_accessor :tags

  def initialize(posts, template, tag)
    posts = posts.filter { |post| post.metadata['tags'].include? tag }
    super(posts, template)
    @metadata = { 'title' => tag, 'description' => self.class.tag_description(tag), type: %w[home tag] }
  end

  def self.tag_description(tag)
    description_filename = File.join('./posts', 'tags', tag.downcase + '.txt')
    return File.read(description_filename) if File.file? description_filename
  end
end
