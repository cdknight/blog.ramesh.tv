require 'date'
require 'logging'
require 'colorize'
require 'cgi'
require 'stringex'
require './renderer/base_post'

class Post < BasePost
  attr_accessor :data, :metadata, :body
  def initialize(pagestr)
    super
    @data = {}
    @metadata = {'type': "post"}
    @body = "<div class='rbp-post'>#{parse_page pagestr}</div>"
  end

  def parse_page(pagestr)
    @logger.debug "(debug) Parser begin.".colorize(:green)

    temp_body = ""
    temp_data = []
    temp_block = ""

    in_block = false
    inline_block = false

    pagestr = pagestr.gsub /\n\n+(?!\()/, "</p><p>"
    pagestr = pagestr.gsub /\A/, "<p>"
    pagestr = pagestr.gsub /\z/, "</p>"

    char_split = pagestr.split('')
    char_split.each_with_index do |char, i|
      if char == "(" and char_split[i-1] != "\\"
        # before_start = char_split.slice(0,i-1)
        # before_start = before_start ? before_start : []

        # if before_start.join('').strip == ""
          # temp_body += "<p>"
        # end
        # temp_body += "</p>"
        in_block = true
        next
      elsif char == ")" and char_split[i-1] != "\\"
        in_block = false

        temp_body += eval_block(temp_block)
        temp_block = ""
        next
      end

      if in_block
        temp_block += char
      else
        temp_body += char
      end

    end

    temp_body = temp_body.gsub(/\\\(/, '(') # allow escape sequences
    temp_body.gsub(/\\\)/, ')')
  end

  def eval_block(block_code)
    commands = block_code.strip.split("\n")
    command_result = ""
    commands.each do |command|
      command_output = eval_command command
      if command_output.class == String
        command_result += command_output
      end
    end
    command_result
  end

  def eval_command(command)
    open_tokens = '"['  # TODO make this a hash, or a separate class
    close_tokens = '"]'
    in_expression = false
    param_temp = ""
    command_args = []
    token_index = 100 # So we get a nil

    command_name = command.split(' ')[0]
    command_chars = command.split(" ")[1..].join(" ").split("") # Everything after the command name

    command_chars.each_with_index do |char, i|
      add = true
      # puts "tokens #{close_tokens[i]}, #{char}".red
      if open_tokens.include? char and not in_expression
        in_expression = true
        token_index = open_tokens.split('').find_index char
      elsif close_tokens[token_index] == char
        in_expression = false
        # puts "adding #{char}".red
        param_temp << char
        token_index = 100
        add = false
      end


      if ((char == "," and not in_expression) || (i+1 == command_chars.length))
        # puts "(debug) param_temp is #{param_temp}".red
        command_args << param_temp
        param_temp = ""
        @logger.debug "(debug) command_args is now #{command_args}"
        add = false
      end

      if add
        param_temp << char
      end

      # puts "(debug) param_temp is #{param_temp}, #{i}, #{command_chars.length}".blue
    end
    # command_args = command.split(' ')[1..].join(" ").split(',')
    command_result = ""

    command_args.each_with_index do |arg, i| # Map doesn't work???
      arg = arg.strip
      if arg[0] == "\"" and arg[-1] == "\""
        # String
        arg = arg[1..-2].gsub(/\\\(/, "(").gsub(/\\\)/, ")") # Replace escaped parentheses in string.
      elsif arg[0] == "[" and arg[-1] == "]"
        # Array
        @logger.debug "turning to array"
        arg = arg[1..-2]
        arg = arg.split(',').map { |el| el.strip[1..-2] } # Only String Arrays supported for now :( Also if you string has a semicolon in it, well, this won't work. TODO recursion
      end

      command_args[i] = arg
    end

    begin
      unless @data[command_name]
        command_result = send "c_" + command_name, command_args
        command_result ||= ""
      else
        command_result = @data[command_name]
      end
    rescue NoMethodError
      @logger.warn "Error: command #{command_name} not found."
    end

    command_result
  end

  def c_set(args)
    @data[args[0]] = args[1]
    @logger.debug "(debug) @data is now #{@data}"
  end

  def c_bold(args)
    "<b>#{args[0]}</b>"
  end

  def c_italic(args)
    "<i>#{args[0]}</i>"
  end

  def c_image(args)
    @logger.debug "(debug) image #{args}"
    data = "<img src=\"#{args[0]}\" alt=\"#{args[1]}\" />"
    unless args[2] == "wrap"
      data = "<p>" + data + "</p>"
    end
    data
  end

  def c_section(args)
    @logger.debug "(debug) section #{args}"

    "<h2>#{args[0]}</h2>"
  end

  def c_list(args)
    @logger.debug "(debug) list #{args}"

    if args[0] == "end"
      return "</ul>"
    end
    "<ul>"
  end

  def c_item(args)
    @logger.debug "(debug) list item #{args}"

    "<li>#{args[0]}</li>"
  end

  def c_link(args)
    @logger.debug "(debug) link item #{args}"

    "<a target='_' href='#{args[0]}'>#{args[1]}</a>"
  end

  def c_code(args)
    @logger.debug "(debug) code item #{args}"

    "<code>#{CGI::escape_html(args[0])}</code>"
  end

  def c_metadata(args)
    if args[0] == "time"
      @metadata[args[0]] = DateTime.parse args[1]
    else
      @metadata[args[0]] = args[1]
    end
    @logger.debug "(debug) @metadata is now #{@metadata}"
  end


end

# page = Page.new(File.read "samplepage.rbp")
