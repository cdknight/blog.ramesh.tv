#!/usr/bin/env ruby

require './config'
require 'rss'

class PostsRSS
  def initialize(posts)
    posts = posts.sort_by { |post| post.metadata['time'] }.reverse
    @rss = RSS::Maker.make('2.0') do |maker|
      maker.channel.title = "#{BlogConfig::BLOG_TITLE}'s RSS Feed"
      maker.channel.link = BlogConfig::BLOG_DOMAIN
      maker.channel.description = BlogConfig::BLOG_DESCRIPTION

      posts.each do |post|
        maker.items.new_item do |item|
          item.link = BlogConfig::BLOG_DOMAIN + '/post/' + post.title_url # TODO: Make a better way to do this
          item.title = post.metadata['title']
          item.updated = post.metadata['time'].to_s
          item.description = post.metadata['description']
        end
      end
    end
  end

  def body
    @rss.to_s
  end
end
