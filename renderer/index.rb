require 'nokogiri'
require 'colorize'
require 'cgi'

class Index
  attr_reader :metadata, :encrypted

  def initialize(posts, template)
    @logger = Logging.logger[self]
    @posts = posts.sort! { |post1, post2| post2.metadata['time'] <=> post1.metadata['time'] }  # TODO: sort by date
    @template = template
    @metadata = { 'title' => "Ramesh's Blog", 'decription' => '', "type": 'home' }
    @body = Nokogiri::XML::NodeSet.new Nokogiri::HTML('<div></div>')
    @type = 'post'

    populate_body
  end

  def populate_body
    @posts.each do |post|
      @body << html_template(post)
    end
  end

  def html_template(post)
    tmp_template = Nokogiri::HTML @template

    # Make clicking on the post "rectangle" work
    if @type == 'post'
      blog_entry = tmp_template.at_css '.blog-entry'
      blog_entry['onclick'] = "Turbolinks.visit('/post/#{post.title_url}')"
    end

    metadata_fill = tmp_template.css 'blogpost'

    metadata_fill.each do |metadata_node|
      if metadata_node['metadata'] == 'time'
        metadata_node.parent.content = post.metadata[metadata_node['metadata']].strftime(metadata_node['format'])
      elsif metadata_node['metadata'] == 'link'
        metadata_node.name = 'a'
        if @type == 'post'
          metadata_node['href'] = "/post/#{post.title_url}"
        elsif @type == 'tag'
          metadata_node['href'] = "/tag/#{post['title'].downcase}" # this is a string tag, with the variable name post
        end
      elsif metadata_node['metadata'] == 'preview'
        if post.encrypted || post.class == FakeEncryptedPost
          metadata_node.parent.parent.remove
        else
          metadata_node.parent.content = CGI.unescapeHTML "#{post.preview}..."
        end
      elsif @type == 'post'
        if post.encrypted
          case metadata_node['metadata']
          when 'description'
            metadata_node.parent.content = "Secret posts are private and not publicly viewable."
          when 'title'
            metadata_node.parent.content = "Secret Post"
          end
        else
          metadata_node.parent.content = post.metadata[metadata_node['metadata']]
        end


      elsif @type == 'tag'
        @logger.debug "tag metadata #{metadata_node['metadata']} #{post[metadata_node['metadata']]}"
        metadata_node.parent.content = post[metadata_node['metadata']] # any other metadata just gets copied
      end
      @logger.debug "(debug index.rb) Metadata node #{metadata_node}"
    end

    tmp_template
  end

  def body
    @body.to_html
  end
end
