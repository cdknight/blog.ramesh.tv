require 'thor'
require 'date'
require 'stringex'

class PostCLI < Thor
  include Thor::Actions
  namespace :post

  desc 'create', 'Create a new post with the supplied arguments.'

  argument :title, type: :string, default: 'New Post', banner: 'title'
  argument :description, type: :string, default: '', banner: 'description'
  argument :tags, type: :array, default: [], banner: 'tags'

  option :ext, type: :string, default: 'org', desc: 'Extension to make this post with', banner: 'extension'

  def create
    filename = title.to_url + ".#{options[:ext]}"
    tags_str = tags.to_a.join(', ')
    time = DateTime.now.strftime '%Y-%m-%d %H:%M'

    template_str = case options[:ext]

                   when 'rbp'
                     %{(
  metadata title, "#{title}"
  metadata description, "#{description}"
  metadata tags, #{tags}
  metadata time, "#{time}"
  metadata language, "en"
)}

                   when 'adoc'

                     %(:description: #{description}
:tags: #{tags_str}
:time: #{time}
:language: en

= #{title})
                   else # pandoc

                     %(---
title: #{title}
description: #{description}
tags: #{tags_str}
time: #{time}
language: en
...

)

                   end

    create_file(File.join('posts', filename), template_str)
  end
end
