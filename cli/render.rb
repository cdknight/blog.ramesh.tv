require './renderer/render'

require 'thor'
require 'logging'
require 'listen'
require 'webrick'

class RenderCLI < Thor
  namespace :render
  option :log_level

  def initialize(*args)
    super
    @logger = Logging.logger[self]
    update_log_level
  end

  desc 'output', 'Render files for production/live website output.'
  def output
    render = Render.new output: true
    render.render
  end

  desc 'encrypted', 'Render encrypted blog posts to partially-encrypted HTML for online builders'
  def encrypted
    @logger.info 'exporting encrypted posts'
    render = Render.new only_encrypted_output: true
    render.clean
    # render_posts automatically outputs to BlogConfig::BLOG_ENCRYPTED_OUTPUT
    # if only_encrypted_output is set to true
    render.render_posts
  end

  desc 'live', 'Render files for debugging/live changes and development.'
  def live
    rebuild
    # Apparently npm seems to keep writing package-lock.json for no reason
    listener = Listen.to(Dir.pwd, ignore: %r{(output/|static/js/.*package-lock.json)}) do |_modified, _added, _removed| 
      @logger.info 'Detected changes, rebuilding the output directory...'.green
      rebuild
    end
    listener.start

    WEBrick::HTTPServer.new(Port: 8000, DocumentRoot: File.join(Dir.pwd, 'output'), AccessLog: []).start
  end

  desc 'clean', 'Remove the output to clean the project.'
  def clean
    render = Render.new
    render.clean
  end

  no_commands do
    def update_log_level
      Logging.logger.root.level = options[:log_level] if options[:log_level]
    end

    def rebuild
      render = Render.new
      render.render

      @logger.info 'Listening for changes...'.green
      @logger.info 'Access your local development webserver at http://localhost:8000'.green
    end
  end
end
